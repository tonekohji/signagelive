function thisthat(initial){
    var tracker = {
        ifThis: {
            service: null,
            trigger: null,
            payload: null
        },
        thenThat: {
            service: null,
            action: null,
            payload: null
        }
    };

    if(!!initial){
        tracker = $.extend(true, tracker, initial);
    }

    tracker.setThisService = function(service){
        if(!!tracker.ifThis){
            tracker.ifThis.service = service;
        } else {
            tracker.ifThis = {
                service: service,
                trigger: null,
                payload: null
            };
        }
    };
    tracker.setThisTrigger = function(trigger){
        if(!!tracker.ifThis){
            tracker.ifThis.trigger = trigger;
        } else {
            tracker.ifThis = {
                service: null,
                trigger: trigger,
                payload: null
            };
        }
    };
    tracker.setThisPayload = function(payload){
        if(!!tracker.ifThis){
            tracker.ifThis.payload = payload;
        } else {
            tracker.ifThis = {
                service: null,
                trigger: null,
                payload: payload
            };
        }
    };
    tracker.setThatService = function(service){
        if(!!tracker.thenThat){
            tracker.thenThat.service = service;
        } else {
            tracker.thenThat = {
                service: service,
                action: null,
                payload: null
            };
        }
    };
    tracker.setThatAction = function(action){
        if(!!tracker.thenThat){
            tracker.thenThat.action = action;
        } else {
            tracker.thenThat = {
                service: null,
                action: trigger,
                payload: payload
            };
        }
    };
    tracker.setThatPayload = function(payload){
        if(!!tracker.thenThat){
            tracker.thenThat.payload = payload;
        } else {
            tracker.thenThat = {
                service: null,
                action: null,
                payload: payload
            };
        }
    };
    tracker.printSummary = function(){
        return "If "+tracker.ifThis.service+" from "+tracker.ifThis.payload+" then "+tracker.thenThat.service + " " + tracker.thenThat.payload;
    };

    return tracker;
}

//Initialization of state tracker
var state = thisthat();

/* Load up all available services and populate service selectors */

//ajax call to simulate API call or database pull
//Services
$.ajax({
    dataType: "json",
    url: "./lib/services/services.json",
    async: false,
    success: function(response) {
        //Populate services
        for(var i=0; i<response.length; i++){
            var service = response[i];
            var name = service.name;
            var code = service.code ? service.code : null;
            var icon = service.icon;
            var bgColor = service.background;
            //This variable is to indicate the services which will not do anything
            var active = service.active;

            if(active){
                $(".services-wrapper", "#step1").append(
                    "<div class='tile service-selector centered actionable' data-code='"+code+"' style='background-color: "+bgColor+"; color: #fff;' data-action='nextStep' data-curstep='1' data-nextstep='2'><i class='fa "+icon+"'></i>"+name+"</div>"
                );
            } else {
                $(".services-wrapper", "#step1").append(
                    "<div class='tile service-selector centered' style='background-color: "+bgColor+"; color: #fff;'><i class='fa "+icon+"'></i> "+name+"</div>"
                );
            }
        }
    }
});
//Action Services
$.ajax({
    dataType: "json",
    url: "./lib/services/actionServices.json",
    async: false,
    success: function(response) {
        //Populate services
        for(var i=0; i<response.length; i++){
            var service = response[i];
            var name = service.name;
            var code = service.code ? service.code : null;
            var icon = service.icon;
            var bgColor = service.background;
            //This variable is to indicate the services which will not do anything
            var active = service.active;

            if(active){
                $(".action-services-wrapper", "#step3").append(
                    "<div class='tile action-service-selector centered actionable' data-code='"+code+"' style='background-color: "+bgColor+"; color: #fff;' data-action='nextStep' data-curstep='3' data-nextstep='4'><i class='fa "+icon+"'></i>"+name+"</div>"
                );
            } else {
                $(".action-services-wrapper", "#step3").append(
                    "<div class='tile action-service-selector centered' style='background-color: "+bgColor+"; color: #fff;'><i class='fa "+icon+"'></i> "+name+"</div>"
                );
            }
        }
    }
});

/* Loading and population END */

function isValidURL(url){
    url = url.trim();
    return /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z‌​]{2,6}\b([-a-zA-Z0-9‌​@:%_\+.~#?&=]*)/.test(url);
}
function isValidEmail(email){
    email = email.trim();
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}
function updateSummary(tracker){
    $(".action-config", "#step6").text(tracker.printSummary());
}

$("form").on("submit", function(e){
    e.preventDefault();
    var form = e.target;
    var inputs = $("input", form);
    $(".alert", form).fadeOut('fast');

    var validForm = true;
    //Proceed with validation of inputs
    for(var i=0; i<inputs.length; i++){
        var toValidate = inputs[i];
        var validInput = true;

        if($(toValidate).hasClass("url-input")){
            var url = toValidate.value.trim();
            validInput = isValidURL(url);
        }
        if($(toValidate).hasClass("email-input")){
            var email = toValidate.value.trim();
            validInput = isValidEmail(email);
        }
        //Add more validation functions for different input types

        if(!validInput){
            validForm = false;
            $("#"+toValidate.getAttribute("name")+"-alert", form).fadeIn('fast');
        }
    }

    if(validForm){
        //Save state in state object
        var forWhat = form.dataset.for;
        var payloadItem = form.dataset.payload;
        var payload = $("input[name="+payloadItem+"]", form)[0].value.trim();

        if(forWhat == "this"){
            state.setThisPayload(payload);
        } else if(forWhat == "that"){
            state.setThatPayload(payload);
        }

        //Go to next step
        var currentStep = "#step"+form.dataset.curstep;
        var nextStep = "#step"+form.dataset.nextstep;
        nextSection(currentStep, nextStep);
    }
    //if valid===true, form will continue submit
});

/* script to activate actionable*/
function nextSection(curSect, nextSect){
    $(curSect).fadeOut('fast', function(){
        $(nextSect).fadeIn('fast');
    });
    updateSummary(state);
}

$(".actionable").on("click", function(e){
    var element = e.currentTarget;
    var currentStep = "#step"+element.dataset.curstep;
    var nextStep = "#step"+element.dataset.nextstep;

    nextSection(currentStep, nextStep);
});

$(".service-selector").on("click", function(e){
    var element = e.currentTarget;
    var service = element.dataset.code;

    state.setThisService(service);
});

$(".trigger-selector").on("click", function(e){
    var element = e.currentTarget;
    var trigger = element.dataset.trigger;

    state.setThisTrigger(trigger);
});

$(".action-service-selector").on("click", function(e){
    var element = e.currentTarget;
    var service = element.dataset.code;

    state.setThatService(service);
});

$(".action-selector").on("click", function(e){
    var element = e.currentTarget;
    var action = element.dataset.action;

    state.setThatAction(action);
});